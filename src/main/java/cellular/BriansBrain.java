package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        this.currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return this.currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row=0; row<this.numberOfRows(); row++){
            for (int col=0; col<this.numberOfColumns(); col++){
                nextGeneration.set(row, col, this.getNextCell(row, col));
            }
        }
        this.currentGeneration = nextGeneration;  
    }

    @Override
    public CellState getNextCell(int row, int col) {
		int aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
		CellState currentState = this.getCellState(row, col);
		CellState nextState = currentState;
		
		if (currentState == CellState.ALIVE) {
			nextState = CellState.DYING;
		}

        else if (currentState == CellState.DYING) {
            nextState = CellState.DEAD;
        }

		else if (currentState == CellState.DEAD) {
			if(aliveNeighbours == 2) {
				nextState = CellState.ALIVE;
			}
		}
		
		return nextState;
	}

    @Override
    public int numberOfRows() {
        return this.currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return this.currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return this.currentGeneration;
    }
    

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int aliveNeighbours = 0;

		for (int modR = row-1; modR<=row+1; modR++) {
			for (int modC = col-1; modC<=col+1; modC++) {
				if (modR == row && modC == col) {
					continue;
				}

				if (modR > this.numberOfRows() || modC > this.numberOfColumns()) {
					continue;
				}

				if (modR < 0 || modC < 0) {
					continue;
				}

				if(this.getCellState(modR, modC) == state) {
					aliveNeighbours++;
				}
			}
		}

		return aliveNeighbours;
	}
}