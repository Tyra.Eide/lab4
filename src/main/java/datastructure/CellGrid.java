package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] CellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.CellGrid = new CellState[rows][columns];

        for (int row=0; row<rows; row++){
            for (int column=0; column<columns; column++){
                this.CellGrid[row][column] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    public void indexCheck(int row, int column){
        if (row > this.rows || column > this.columns) {
            throw new IndexOutOfBoundsException();
        }

        else if (row < 0 || column < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void set(int row, int column, CellState element) {
        indexCheck(row, column);
        this.CellGrid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        indexCheck(row, column);
        return this.CellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid CellCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row=0; row<this.rows; row++) {
            for (int column=0; column<this.columns; column++) {
                CellCopy.set(row, column, this.get(row, column));
            }
        }
        return CellCopy;
    }
    
}
